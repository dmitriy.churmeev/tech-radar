import React, {useContext} from 'react';
import TextWrapper from './Text.style';
import {ThemeContext} from '../theme-context';

interface Props {
    name: string;
    dx: number;
    dy:number;
}

function Text({
    name,
    dx,
    dy
}: Props) {
    // context variables
    const {fontSize, fontFamily} = useContext(ThemeContext);

    return (
        <TextWrapper
            className="quadrant"
            fontSize={fontSize}
            fontFamily={fontFamily}
            dx={dx}
            dy={dy}
        >
            {name}
        </TextWrapper>
    );
}

export default Text;
