import React from 'react';

interface Props {
    x2: number;
    y2: number;
    stroke: string;
}

function Line({x2, y2, stroke}: Props) {
    return (
        <line
            x1="0"
            y1="0"
            x2={x2}
            y2={y2}
            stroke={stroke}
        />
    );
}

export default Line;
