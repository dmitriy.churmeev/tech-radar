import React from 'react';
import RequestWrapper from './RequestWrapper';
import Radar from './Libraries/ReactRadar/Radar/Radar';
import List from './RadarComponent/assets/List';

function RenderRadarComponent() {
    // render radar & radar
    return RequestWrapper((data: any) => {
        return (
            <div className="radar__container">
                <List {...data} />
                <Radar {...data} />
            </div>
        );
    });
}

export default RenderRadarComponent;
