import {setup} from './RadarComponent/assets/data/staticRadarSetup';

export default function RequestWrapper(callback: any) {
    // Описать запрос
    return callback(setup);
}
