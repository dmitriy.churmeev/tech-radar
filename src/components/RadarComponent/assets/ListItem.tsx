import React from 'react';
import ListItemHandler from './ListItemHandler';

interface Props {
    item: {
        name: string;
        quadrant: number;
    };
    name: string;
    onChangeState: (e: any, active: boolean) => void;
    active: boolean;
}

export default function ListItem({
    item,
    onChangeState,
    active
}: Props) {
    return (
        <div
            data-name={item.name}
            data-quadrant={item.quadrant}
            className={active ? 'list__item active' : 'list__item'}
            onClick={(e) => {
                onChangeState(e.target, !active);
                ListItemHandler(item);
            }}
        >
            { item.name }
        </div>
    );
}
