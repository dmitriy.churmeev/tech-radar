import React from 'react';
import RenderRadarComponent from './components/RenderRadarComponent';

function App() {
    return (
        <RenderRadarComponent />
    );
}

export default App;
